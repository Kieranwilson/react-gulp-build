// Defining requirements
var plumber = require('gulp-plumber');
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var gulpSequence = require('gulp-sequence');
var browserify = require('browserify');
var watchify = require('watchify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');

// browser-sync watched files
// automatically reloads the page when files changed
var browserSyncWatchFiles = [
'./css/**',
'./js/**',
'./*.html'
];

// browser-sync options
// see: https://www.browsersync.io/docs/options/
var browserSyncOptions = {
    proxy: "http://localhost/react/week-2",
    notify: false
};

// Run:
// gulp sass
// Compiles SCSS files in CSS
gulp.task('sass', function () {
    return gulp.src('./sass/*.scss')
    .pipe(plumber({
        errorHandler: function (err) {
            console.log(err);
            this.emit('end');
        }
    }))
    .pipe(sass())
    .pipe(gulp.dest('./css'));
});



browserify("react/app.js")
        .transform("babelify", {presets: ["es2015", "react"]})
// Run:
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes
gulp.task('watch', function () {
    gulp.watch('./sass/*.scss', ['styles']);

    var watcher  = watchify(
        browserify("react/app.js")
            .transform("babelify", {presets: ["es2015", "react"]})
    );

    return watcher.on('update', function () {
        watcher.bundle()
            .on('error', function (err) {
                console.log(err.toString());
                this.emit("end");
            })
            .pipe(source('app.js'))
            .pipe(gulp.dest('js'))
            console.log('Updated');
    }).bundle()
        .on('error', function (err) {
            console.log(err.toString());
            this.emit("end");
        })
        .pipe(source('app.js'))
        .pipe(gulp.dest('js'))
});

gulp.task('minify-css', function() {
    return gulp.src('./css/style.css')
        .pipe(plumber({
            errorHandler: function (err) {
               console.log(err);
                this.emit('end');
            }
        }))
        .pipe(cleanCSS({compatibility: '*'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./css/'));
});

gulp.task('styles', function(callback){ gulpSequence('sass', 'minify-css')(callback) });

// Run:
// gulp browser-sync
// Starts browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    browserSync.init(browserSyncWatchFiles, browserSyncOptions);
});

// Run:
// gulp watch-bs
// Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
gulp.task('watch-bs', ['browser-sync', 'watch'], function () { });

gulp.task('default', ['watch-bs']);

gulp.task('build-js', function() {
    return browserify("react/app.js")
        .transform("babelify", {presets: ["es2015", "react"]})
        .bundle()
        .pipe(plumber({
            errorHandler: function (err) {
               console.log(err);
                this.emit('end');
            }
        }))
        .pipe(source('app.js'))
        .pipe(gulp.dest('js'));
});

gulp.task('minify-js', function() {
    return browserify("react/app.js")
        .transform("babelify", {presets: ["es2015", "react"]})
        .bundle()
        .pipe(plumber({
            errorHandler: function (err) {
               console.log(err);
                this.emit('end');
            }
        }))
        .pipe(source('app.min.js'))
        .pipe(streamify(uglify('app.min.js')))
        .pipe(gulp.dest('js'));

});

gulp.task('scripts', function(callback){ gulpSequence('build-js', 'minify-js')(callback) });