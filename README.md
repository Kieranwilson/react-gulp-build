# React Gulp Tasks

> React Gulp build task

I signed up for a four week React course. The course itself was way too slow paced, 
but I went down a rabbit hole in the second week because I wanted to create the same
build tasks and component structure that I like to follow for my front end projects.

Creating this build task took me most of a day but it has everything I would need for 
any future react projects including browsersync for hot reload of the JS and the SASS.

The only thing that I would add to this is the gulp ruby-sass as it includes sourcemaps
 which are amazing to have, and will add this in the near future when I create a react project.