var React = require('react');
var ReactDOM = require('react-dom');
var Card = require('./Components/Card');

import person from './data/person';

ReactDOM.render(
  <div>
    <Card person={person} />
  </div>,
  document.getElementById('app')
);
