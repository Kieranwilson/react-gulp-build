var React = require('react');

class Photo extends React.Component {
	render() {
		return (
			<div className="photo">
				<img src={this.props.photo} alt="Photo" />
			</div>
		);
	}
}

module.exports = Photo;