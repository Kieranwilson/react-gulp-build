var React = require('react');

class Bio extends React.Component {
	render () {
		return (
			<div className="bio">
				<h1 className="name">{this.props.name}</h1>
				<h2 className="location">{this.props.location}</h2>
				<div className="occupation">
					<p>{this.props.occupation.title} at {this.props.occupation.employer}</p>
				</div>
			</div>
		)
	}
}

module.exports = Bio