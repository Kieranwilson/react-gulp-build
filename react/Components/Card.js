var React = require('react');
var Photo = require('./Photo');
var Bio = require('./Bio');
var Updates = require('./Updates');

class Card extends React.Component {
	render () {
		return(
			<div className="card">
				<Photo photo={this.props.person.photo} />
				<Bio name={this.props.person.name} location={this.props.person.location} occupation={this.props.person.occupation} />
				<Updates updates={this.props.person.updates} />
			</div>
		)
	}
}

module.exports = Card