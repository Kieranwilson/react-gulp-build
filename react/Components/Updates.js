var React = require('react');

class Updates extends React.Component {
	updates () {
		return this.props.updates.map(function(update,index){
			return (
				<li className={"update " + update.platform} key={index}>
					{update.status}
				</li>
			)
		});
	}

	render () {
		return(
			<div className="updates">
				<ul>
					{this.updates()}
				</ul>
			</div>
		)
	}
}

module.exports = Updates;